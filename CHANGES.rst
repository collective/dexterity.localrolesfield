Changelog
=========

0.2 (unreleased)
----------------

- Nothing changed yet.


0.1 (2014-10-24)
----------------

- Initial release
  [mpeeters]
